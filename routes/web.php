<?php

use App\Http\Controllers\BlogController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
        return redirect()->route('login');
});

Route::middleware(['auth'])->group(function () {
        Route::get("/Dashboard", [HomeController::class, "Dashboard"])->name("Dashboard");

        Route::prefix("blog")->group(function () {
                Route::get("/", [BlogController::class, "index"])->name("blog.index");
                Route::get("/create", [BlogController::class, "create"])->name("blog.create");
                Route::post("/store", [BlogController::class, "store"])->name("blog.store");
                Route::get("/edit/{id}", [BlogController::class, "edit"])->name("blog.edit");
                Route::post("/update", [BlogController::class, "update"])->name("blog.update");
                Route::get("/delete/{id}", [BlogController::class, "destroy"])->name("blog.delete");
        });
});
Auth::routes();