<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function Dashboard(Request $request)
    {
        $data = [
            "first_name" => "ramesh",
            "last_name" => "Jadav",
            "email" => "rameshjadav133@gmail.com"
        ];
        return Inertia::render("Dashboard/index", $data);
    }
}
