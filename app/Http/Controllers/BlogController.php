<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs =  Blog::orderBy("id","desc")->paginate(2);
        return Inertia::render('Blogs/index', [
            'blogs' => $blogs,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render("Blogs/create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'title' => ['required'],
            'sub_title' => ['required'],
            'image' => ['required'],
            'description' => ['required', 'max:200'],
        ]);

        $blog = new Blog;
        $blog->title =  $request->title;
        $blog->sub_title =  $request->sub_title;
        $blog->image =  $request->image;
        $blog->description =  $request->description;
        $blog->save();

        return Redirect::route('blog.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blog::find($id);
        return Inertia::render("Blogs/edit",["blog" => $blog]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
        $validate = $request->validate([
            'id' => ['required'],
            'title' => ['required'],
            'sub_title' => ['required'],
            'image' => ['required'],
            'description' => ['required', 'max:200'],
        ]);

        $blog = Blog::find($request->id);
        $blog->title =  $request->title;
        $blog->sub_title =  $request->sub_title;
        $blog->image =  $request->image;
        $blog->description =  $request->description;
        $blog->save();

        return Redirect::route('blog.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Blog::destroy($id);
        return Redirect::route('blog.index');
    }
}
